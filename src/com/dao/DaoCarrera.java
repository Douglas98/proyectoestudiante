/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dao;

import com.conexion.Conexion;
import com.interfaces.CarreraInterface;
import com.modelo.Carrera;
import com.modelo.Facultad;
import java.util.List;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Nombre de la clase: DaoCarrera
 * Version: 1.0;
 * Fecha: 05/10/2019
 * Copyright: EnclaveStudio
 * @author Oscar Caceres
 */
public class DaoCarrera extends Conexion implements CarreraInterface  {
    String mensaje = "";
    String sql = "";
    @Override
    public String insertar(Carrera car) throws Exception{
        try {
            this.conectar();
            sql = "insert into carrera values(?,?,?,?)";
            PreparedStatement pre = this.getCnn().prepareStatement(sql);
            pre.setInt(1, car.getCodigoCarrera());
            pre.setString(2, car.getNombre());
            pre.setInt(3, car.getCantidadUv());
            pre.setInt(4, car.getCodigoFacultad());
            pre.executeUpdate();
            mensaje = "Datos ingresados correctamente";
        } catch (Exception e) {
            
        }finally{
            this.desconectar();
        }
        return mensaje;
    }

    @Override
    public String modificar(Carrera car) throws Exception {
        try {
            this.conectar();
            sql = "update carrera set nombre = ?, cantidadUv = ?, codigoFacultad = ? where codigoCarrera = ?";
            PreparedStatement pre = this.getCnn().prepareStatement(sql);
            pre.setString(1, car.getNombre());
            pre.setInt(2, car.getCantidadUv());
            pre.setInt(3, car.getCodigoFacultad());
            pre.setInt(4, car.getCodigoCarrera());
            pre.executeUpdate();
            mensaje = "Se modifico correctamente";
        } catch (SQLException e) {
            mensaje = e.toString();
        }finally{
            this.desconectar();
        }
        return mensaje;
    }

    @Override
    public String eliminar(Carrera car) throws Exception {
        
        try {
            this.conectar();
           sql = "Delete from carrera where codigoCarrera = ?" ;
           PreparedStatement pre = this.getCnn().prepareStatement(sql);
           pre.setInt(1, car.getCodigoCarrera());
           pre.executeUpdate();
           mensaje = "Datos eliminados correctamente";
           
        } catch (Exception e) {
            mensaje = e.toString();
        }finally{
            this.desconectar();
        }
        return mensaje;
        
    }
    public List<Facultad> retornarFacultad() throws Exception{
        List<Facultad> lista = new ArrayList();
        ResultSet res;
        try {
               this.conectar();
               sql = "Select *from facultad";
               PreparedStatement pre = this.getCnn().prepareStatement(sql);
               res = pre.executeQuery();
               while(res.next()){
                   Facultad f = new Facultad();
                   f.setCodigoFacultad(res.getInt("codigoFacultad"));
                   f.setNombre(res.getString("nombre"));
                   f.setTelefono(res.getString("telefono"));
                   lista.add(f);
               }
               
        } catch (Exception e) {
        }finally{
            this.desconectar();
        }
        return lista;
    }
            
            
    
}
