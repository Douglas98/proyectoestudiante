/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dao;

import com.conexion.Conexion;
import com.modelo.Carrera;
import com.modelo.Estudiante;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

/**
 * Nombre de la clase: DaoEstudiante
 * Version: 1.0
 * Fecha: 06/10/2019
 * Copyright: EnclaveStudio
 * @author Oscar Caceres
 */
public class DaoEstudiante extends Conexion{
   String mensaje = "";
   String sql = "";
    public String insertar(Estudiante es) throws Exception{
        try {
            this.conectar();
            sql = "INSERT INTO estudiante VALUES(?,?,?,?,?,?,?,?)";
            PreparedStatement pre = this.getCnn().prepareStatement(sql);
            pre.setInt(1, es.getCodigoEstudiante());
            pre.setString(2, es.getNombre());
            pre.setInt(3, es.getEdad());
            pre.setString(4, es.getGenero());
            pre.setDouble(5, es.getCumCiclo());
            pre.setString(6, es.getDireccion());
            pre.setString(7, es.getTelefonoMovil());
            pre.setInt(8, es.getCodigoCarrera());
            pre.executeUpdate();
            mensaje = "Se inserto correctamente";             
        } catch (SQLException e) {
            mensaje = "Error al insertar: " + e.getMessage();
            
        }finally{
            this.desconectar();
        }
        return mensaje;
    }
    public String modificar(Estudiante es) throws Exception {
        try {
            this.conectar();
            sql = "UPDATE estudiante SET nombre = ?, edad = ?, genero=?,cumCiclo = ?, direccion = ?, telefonoMovil=?,codigoCarrera=? WHERE codigoEstudiante = ?";
            PreparedStatement pre = this.getCnn().prepareStatement(sql);
            pre.setString(1, es.getNombre());
            pre.setInt(2, es.getEdad());
            pre.setString(3, es.getGenero());
            pre.setDouble(4, es.getCumCiclo());
            pre.setString(5, es.getDireccion());
            pre.setString(6, es.getTelefonoMovil());
            pre.setInt(7, es.getCodigoCarrera());
            pre.setInt(8, es.getCodigoEstudiante());
            pre.executeUpdate();
            mensaje = "Se modifico correctamente";
        } catch (SQLException e) {
            mensaje = "Error al modificar " + e.getMessage();
        }finally{
            this.desconectar();
        }
        return mensaje;
    }
    public String eliminar(Estudiante es) throws Exception{
        try {
            this.conectar();
            sql = "DELETE FROM estudiante WHERE codigoEstudiante = ?";
            PreparedStatement pre = this.getCnn().prepareStatement(sql);
            pre.setInt(1, es.getCodigoEstudiante());
            pre.executeUpdate();
            mensaje = "Se elimino correctamente";
        } catch (Exception e) {
            mensaje = "Error al eliminar: " + e.getMessage();
        }finally{
            this.desconectar();
        }
        return mensaje;
    }
    
    public List<Carrera> mostrarCarrera() throws Exception
    {
       List<Carrera> listaP = new ArrayList(); 
       ResultSet rs;
        try 
        {
            this.conectar();
            String sql = "Select *from carrera";
            PreparedStatement pre = this.getCnn().prepareStatement(sql);
            rs=pre.executeQuery();
            while (rs.next())
            {
                Carrera c = new Carrera();
                c.setCodigoCarrera(rs.getInt("codigoCarrera"));
                c.setNombre(rs.getString("nombre"));
                c.setCantidadUv(rs.getInt("cantidadUv"));
                c.setCodigoFacultad(rs.getInt("codigoCarrera"));
                listaP.add(c);
            }
            
        } 
        catch (SQLException e) 
        {
            JOptionPane.showMessageDialog(null, "Error al mostrar " + e.getMessage());
        }
        finally
        {
            this.desconectar();
        }
        
        return listaP;
    }
}
