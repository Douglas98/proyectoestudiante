/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dao;

import com.conexion.Conexion;
import com.modelo.Facultad;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author douglas
 */
public class DaoFacultad extends Conexion{
    
    public String insertarFacultad(Facultad f) throws Exception
    {
        try 
        {
            this.conectar();
            String sql="";
            sql = "insert into facultad values(?,?,?);";
            PreparedStatement pre = this.getCnn().prepareStatement(sql);
            pre.setInt(1, f.getCodigoFacultad());
            pre.setString(2, f.getNombre());
            pre.setString(3, f.getTelefono());
            pre.executeUpdate();
        } 
        catch (Exception ex) 
        {
           throw ex;
        }
        finally
        {
            this.desconectar();
        }
        
        return "Facultad Insertada Correctamente";
    }
    
    
    
    
     public String modificarFacultad(Facultad f) throws Exception
        {
            try 
            {
                this.conectar();
                String sql="";
                sql = "update facultad set nombre=?,telefono=? where codigoFacultad=?";
                PreparedStatement pre = this.getCnn().prepareStatement(sql);
                pre.setString(1, f.getNombre());
                pre.setString(2, f.getTelefono());
                 pre.setInt(3, f.getCodigoFacultad());
                pre.executeUpdate();
            } 
            catch (Exception ex) 
            {
               throw ex;
            }
            finally
            {
                this.desconectar();
            }

            return "Facultad Modificada Correctamente";
        }
    
     
      public String EliminarFacultad(Facultad f) throws Exception
        {
            try 
            {
                this.conectar();
                String sql="";
                sql = "delete from facultad where codigoFacultad=?";
                PreparedStatement pre = this.getCnn().prepareStatement(sql);
                pre.setInt(1, f.getCodigoFacultad());
                pre.executeUpdate();
            } 
            catch (Exception ex) 
            {
               throw ex;
            }
            finally
            {
                this.desconectar();
            }

            return "Facultad Eliminada Correctamente";
        }
}
