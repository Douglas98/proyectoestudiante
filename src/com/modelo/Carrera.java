/**
 * Nombre de la clase: Carrera;
 * Copyright: EnclaveStudio
 * Fecha: 05/10/2019;
 * Version: 1.0
 * @author Oscar caceres, Douglas Menjivar
 */
package com.modelo;

/**
 *
 * @author douglas
 */
public class Carrera {
    private int codigoCarrera;
    private String nombre;
    private int cantidadUv;
    private int codigoFacultad;

    public Carrera() {
    }

    public Carrera(int codigoCarrera, String nombre, int cantidadUv, int codigoFacultad) {
        this.codigoCarrera = codigoCarrera;
        this.nombre = nombre;
        this.cantidadUv = cantidadUv;
        this.codigoFacultad = codigoFacultad;
    }

    public int getCodigoCarrera() {
        return codigoCarrera;
    }

    public void setCodigoCarrera(int codigoCarrera) {
        this.codigoCarrera = codigoCarrera;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getCantidadUv() {
        return cantidadUv;
    }

    public void setCantidadUv(int cantidadUv) {
        this.cantidadUv = cantidadUv;
    }

    public int getCodigoFacultad() {
        return codigoFacultad;
    }

    public void setCodigoFacultad(int codigoFacultad) {
        this.codigoFacultad = codigoFacultad;
    }
    
    
    
}
