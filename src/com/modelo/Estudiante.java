/**
 * Nombre de la clase: Estudiante;
 * Copyright: EnclaveStudio
 * Fecha: 05/10/2019;
 * Version: 1.0
 * @author Oscar caceres, Douglas Menjivar
 */
package com.modelo;


public class Estudiante {
    
    private int codigoEstudiante;
    private String nombre;
    private int edad;
    private String genero;
    private double cumCiclo;
    private String direccion;
    private String telefonoMovil;
    private int codigoCarrera;

    public Estudiante() {
    }

    public Estudiante(int codigoEstudiante, String nombre, int edad, String genero, double cumCiclo, String direccion, String telefonoMovil, int codigoCarrera) {
        this.codigoEstudiante = codigoEstudiante;
        this.nombre = nombre;
        this.edad = edad;
        this.genero = genero;
        this.cumCiclo = cumCiclo;
        this.direccion = direccion;
        this.telefonoMovil = telefonoMovil;
        this.codigoCarrera = codigoCarrera;
    }

    public int getCodigoEstudiante() {
        return codigoEstudiante;
    }

    public void setCodigoEstudiante(int codigoEstudiante) {
        this.codigoEstudiante = codigoEstudiante;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public double getCumCiclo() {
        return cumCiclo;
    }

    public void setCumCiclo(double cumCiclo) {
        this.cumCiclo = cumCiclo;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefonoMovil() {
        return telefonoMovil;
    }

    public void setTelefonoMovil(String telefonoMovil) {
        this.telefonoMovil = telefonoMovil;
    }

    public int getCodigoCarrera() {
        return codigoCarrera;
    }

    public void setCodigoCarrera(int codigoCarrera) {
        this.codigoCarrera = codigoCarrera;
    }
    
    
    
}
