/**
 * Nombre de la clase: MetodosUtil;
 * Copyright: EnclaveStudio
 * Fecha: 05/10/2019;
 * Version: 1.0
 * @author Oscar caceres, Douglas Menjivar
 */
package com.utilidades;

import com.conexion.Conexion;
import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;


public class MetodosUtil extends Conexion{
    public void llenarGrid(JTable tabla, String sql, String[] headers) throws Exception{
        DefaultTableModel modelo = new DefaultTableModel(null, headers);
        ResultSet res;
        ResultSetMetaData rem;
        try {
            this.conectar();
            PreparedStatement pre = this.getCnn().prepareStatement(sql);
            res = pre.executeQuery();
            rem = res.getMetaData();
            int filas = rem.getColumnCount();
            while(res.next()){
                Object[] obj = new Object[filas];
                for (int i = 0; i < filas; i++) {
                    obj[i] = res.getObject(i+1);
                }
                modelo.addRow(obj);
            }
            tabla.setModel(modelo);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al mostrar los datos: " + e.toString());
        }finally{
            this.desconectar();
        }
    }
    public void llenarCombo(JComboBox combo, String sql, String campoInt, String campoString) throws Exception{
        ResultSet res;  
        try {
            this.conectar();
            PreparedStatement pre = this.getCnn().prepareStatement(sql);
            res = pre.executeQuery();
            while(res.next()){
                combo.addItem(new ComboItem(res.getInt(campoInt), res.getString(campoString)));
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,"Error al llenar los combos: " + e.toString());
        }finally{
            this.desconectar();
        }
    }
    
    public void soloNumeros(KeyEvent evt){
        Character c = evt.getKeyChar();
        if(!Character.isDigit(c)){
            evt.consume();
        }
    }
    
    public void soloLetras(KeyEvent evt){
        Character c = evt.getKeyChar();
        if(!Character.isLetter(c) && c != KeyEvent.VK_SPACE)
        {
            evt.consume();
        }
    }
    
    public void soloDecimales(JTextField caja, KeyEvent evt){
        Character c = evt.getKeyChar();
        if(!Character.isDigit(c) && c != '.'){
            evt.consume();
        }
        if(c == '.' && caja.getText().contains(".")){
            evt.consume();
        }
    }
    public void centrarFormularios(JDesktopPane desktop, JInternalFrame frame){
          Dimension desktopSize = desktop.getSize();
         Dimension jInternalFrameSize = frame.getSize();
          frame.setLocation((desktopSize.width - jInternalFrameSize.width)/2,
          (desktopSize.height- jInternalFrameSize.height)/2);
    }
}
