/**
 * Nombre de la clase: ComboItem;
 * Copyright: EnclaveStudio
 * Fecha: 05/10/2019;
 * Version: 1.0
 * @author Oscar caceres, Douglas Menjivar
 */
package com.utilidades;


public class ComboItem {
    private int value;
    private String label;

    public ComboItem(int value, String label) {
        this.value = value;
        this.label = label;
    }

    public int getValue() {
        return value;
    }

    public String getLabel() {
        return label;
    }

    @Override
    public String toString() {
        return label;
    }
    
    
}
