/**
 * Nombre de la clase: Conexion;
 * Copyright: EnclaveStudio
 * Fecha: 05/10/2019;
 * Version: 1.0
 * @author Oscar caceres
 */
package com.conexion;

import java.sql.Connection;
import java.sql.DriverManager;


public class Conexion {
    private Connection cnn;

    public Connection getCnn() {
        return cnn;
    }
    
    public void conectar() throws Exception{
        try {
            Class.forName("com.mysql.jdbc.Driver");
            cnn = DriverManager.getConnection("jdbc:mysql://localhost:3306/Estudiante?user=root&password=Realmadrid98");
        } catch (Exception e) {
            throw e;
        }
    }
    public void desconectar() throws Exception{
        try {
            if(cnn != null){
                if(cnn.isClosed() == false){
                    cnn.close();
                }
            }
        } catch (Exception e) {
            throw e;
        }
    }
}
