/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.interfaces;

import com.modelo.Carrera;

/**
 *  Nombre de la interface: CarreraInterface
 *  Version: 1.0
 *  Fecha: 06/10/2019
 * Copyright: Enclave Studio
 * @author Oscar caceres
 */
public interface CarreraInterface {
    public abstract String insertar(Carrera car) throws Exception;
    public abstract String modificar(Carrera car) throws Exception;
    public abstract String eliminar(Carrera car) throws Exception;
}
